// 
// console.log("Hello World!")
/*


*/

// let productName = 'desktop computer';
// console.log(productName);
// productName = 'cellphone';
// console.log(productName);

// let productPrice = 500;
// console.log(productPrice);
// productPrice = 450;
// console.log(productPrice);

// // CONSTANTS
// /*


// */

// const deliveryFee = 30;
// console.log(deliveryFee);

// DATA TYPES

// 1. STRINGS
/*
	- Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT"


*/
let country = 'Philippines';
let province = "Metro Manila";

// CONCATENATION
console.log(country + "," + province);

// 2. NUMBERS
/* 
	- Include integers/whole numbers, decimal numbers, fraction, exponential notation
*/

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade);

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);

console.log(Math.PI);

console.log("John's grade last quarter is: " + grade);

// 3. BOOLEAN
/*

 */
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);
console.log(isMarried)
console.log(isGoodConduct);

// 4. OBJECTS

	// Arrays
	/* 

	*/

	let grades = [98.7, 92.1, 90.2, 94.6];
	console.log(grades)
	console.log(grades[0])

	let userDetails = ["John", "Smith", 32, true]
	console.log(userDetails);
















